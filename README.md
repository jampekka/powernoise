# Generate 2D power noise images

Generates noise images that have power spectrum `P(f) = 1/f^beta`.
Mostly based on [Timmer, J. and Koenig, M., 1995. On generating power law noise.](http://adsabs.harvard.edu/full/1995A%26A...300..707T).

## Samples
Create your own with

    python2 powernoise.py -s 256 -b <beta> -o <output>.png

Or run without output for interactive demo

    python2 powernoise.py -b <beta> -f

| Type | Result |
| -----|--------|
| "White noise" (beta=0.0) | ![](doc/beta0.png) |
| "Pink noise" (beta=1.0) | ![](doc/beta1.png) |
| "Brownian noise" (beta=2.0) | ![](doc/beta2.png) |


