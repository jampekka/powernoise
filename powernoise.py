import numpy as np
import scipy.ndimage

def pownoise2d(size=1024, beta=1.0):
    # Mostly based on http://adsabs.harvard.edu/full/1995A%26A...300..707T
    # Although we cheat a bit and skip the symmetry and just
    # truncate the imaginary parts in the end result.
    freqs = np.fft.fftfreq(size)
    HF = np.zeros((size, size), dtype=np.complex)
    for xi, xf in enumerate(freqs):
        for yi, yf in enumerate(freqs):
            x = np.random.randn(2)
            freq = np.sqrt(xf**2 + yf**2)
            if freq == 0:
                x = (1.0, 0)
            else:
                x *= (1/freq)**(beta/2.0)
            HF[xi, yi] = complex(*x)
    x = np.real(np.fft.ifft2(HF))
    return x

import scipy.misc
def save_png_8bit(output, x):
    scipy.misc.imsave(output, (x*256).astype(np.uint8))

def save_png_floathack(output, x):
    import matplotlib.pyplot as plt
    enc = (x*256**4).astype(np.uint32)
    w, h = x.shape
    enc = np.frombuffer(enc.tobytes(), dtype=np.uint8)
    enc = enc.reshape(w, h, 4)
    scipy.misc.imsave(output, enc)

    #plt.imshow(enc[:,:,-1])
    #plt.show()

def demo(size=1024, beta=1.0, freqs_plot=False, output=None, pack_float=False):
    import matplotlib.pyplot as plt
    x = pownoise2d(size, beta)
    x = x.astype(np.float)
    x /= np.std(x)*5
    x = (np.arctan(x) + 1)/2

    plt.imshow(x, cmap=plt.cm.gray)
    if output:
        if not pack_float:
            save_png_8bit(output, x)
        else:
            save_png_floathack(output, x)
        return
    if not freqs_plot:
        plt.show()
        return
    plt.figure()
    freqs = np.fft.fftfreq(size)
    power = np.abs(np.fft.fft2(x))**2
    power = power[:,0]
    plt.loglog()
    rng = slice(1, int(size/2))
    plt.plot(freqs[rng], power[rng])
    ideal = (1/freqs[rng])**(beta)
    ideal *= np.sum(power[rng])/np.sum(ideal)
    plt.plot(freqs[rng], ideal, color='red')
    plt.show()

if __name__ == '__main__':
    import argh
    argh.dispatch_command(demo)

